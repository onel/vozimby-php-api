<?php

namespace Vozimby\Exception;

/**
 * Remote service fail exception
 */
class ServiceErrorException extends \Exception
{
}