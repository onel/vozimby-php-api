<?php

namespace Vozimby;

use Vozimby\Client;
use Vozimby\CdtInterface;
use Vozimby\Exception\InsufficientDataException;
use Vozimby\Exception\ServiceErrorException;
use Vozimby\Exception\NotFoundException;

/**
 * Receipt connector
 */
class Receipt implements CdtInterface
{
    /**
     * Status contstant
     */
    const ST_STORE = 0;
    const ST_DELIVERY = 1;
    const ST_RETURN = 2;
    const ST_DELIVERED = 4;
    const ST_OFFICE_ISSUED = 5;
    const ST_CONFISCATED = 6;
    const ST_STORE_RETURN = 7;
    const ST_STORE_RETURN_ISSUED = 8;
    const ST_CLOSED = 9;
    const ST_CLOSED_ISSUED = 26;
    const ST_FILLED = 10;
    const ST_OFFICE_RETURN = 11;
    const ST_DRIVER_RETURN = 12;
    const ST_OFFICE_RETURN_ISSUED = 13;
    const ST_DRIVER_INTAKE = 14;
    const ST_OFFICE_INTAKE = 15;
    const ST_STORE_INTAKE = 16;
    const ST_OFFICE_INTAKE_ISSUED = 17;
    const ST_STORE_INTAKE_ISSUED = 18;
    const ST_DUPLICATED = 19;
    const ST_ONBRANCH = 20;
    const ST_BRANCH_DRIVER = 21;
    const ST_RB_RD_RECEIVE = 22;
    const ST_RB_RD_DELIVERY = 23;
    const ST_RB_RD_REDIRECT = 24;
    const ST_RB_RD_CANSELED = 25;


    /**
     * Receipt UID
     */
    private $id;

    /**
     * Receipt series
     */
    private $series;

    /**
     * Receipt number
     */
    private $number;

    /**
     * attributes list
     */
    private $attributes;

    /**
     * service client
     */
    private $client = null;

    /**
     * @param Client $client service client
     * @param string $series receipt series
     * @param string $number receipt number
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * set sn
     * @return this
     */
    public function setSN($series, $number)
    {
        $this->series = $series;
        $this->number = $number;
        return $this;
    }

    /**
     * set receipt UID
     * @return this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * setup receipt attrs
     *
     * @param $this
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    /**
     * setup single attr
     *
     * @param $this
     */
    public function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
        return $this;
    }

    /**
     * validate receipt
     *
     * TODO
     *
     * @return boolean
     */
    private function isValid()
    {
        return true;
    }

    /**
     * query remote
     *
     * @return this
     */
    public function update()
    {
        if(empty($this->id)
        || empty($this->attributes)
        || !$this->isValid()) {
            throw new InsufficientDataException('setup all attributes');
        }

        $attrs = $this->attributes;
        $attrs['id'] = $this->id;

        $options = array(
            'r' => 'api/receiptCRU',
            'Receipt' => $attrs,
        );

        $response = $this->client->request($options);

        if($response['code'] == 400) {
            throw new InsufficientDataException(var_export($response['errors'], true));
        }
        else if($response['status'] != 'ok') {
            throw new ServiceErrorException(var_export($response, true));
        }

        return $this;
    }

    /**
     * query remote - force create
     *
     * @return integer receipt ID
     */
    public function create()
    {
        if(empty($this->attributes)
        || !$this->isValid()) {
            throw new InsufficientDataException('setup all attributes');
        }

        $attrs = $this->attributes;

        if(isset($attrs['id']))
            unset($attrs['id']);

        $options = array(
            'r' => 'api/receiptCRU',
            'Receipt' => $attrs,
        );

        $response = $this->client->request($options);

        if($response['code'] == 400) {
            throw new InsufficientDataException(var_export($response['errors'], true));
        }
        else if($response['status'] != 'ok') {
            throw new ServiceErrorException(var_export($response, true));
        }

        $this->id = $response['id'];

        return $response['id'];
    }


    /**
     * check reseipt status
     *
     * @throws InsufficientDataException
     * @throws ServiceErrorException
     * @throws NotFoundException
     *
     * @return integer status code
     */
    public function getStatus()
    {
        if((empty($this->series) || empty($this->number)) && empty($this->id)) {
            throw new InsufficientDataException('setup series and number');
        }

        $options = array(
            'r' => 'api/statusJson',
        );

        if($this->id)
        {
            $options['id'] = $this->id;
        }
        else
        {
            $options['series'] = $this->series;
            $options['number'] = $this->number;
        }

        $response = $this->client->request($options);

        if($response['status'] != 'ok') {
            throw new NotFoundException($response['error_message']);
        }

        return $response['status'];
    }
}
