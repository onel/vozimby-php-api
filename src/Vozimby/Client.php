<?php

namespace Vozimby;

use Curl\Curl;
use Vozimby\Exception\ServiceErrorException;

/**
 * Service client
 */
class Client
{
    /**
     * remote gate
     */
    private $remote = 'http://b2b.vozim.by/index.php';

    /**
     * auth name
     */
    private $username;

    /**
     * auth sha256 password
     */
    private $password_hash;


    /**
     * @param string http remote gateway
     */
    public function __construct($remote=null)
    {
        if($remote)
            $this->remote = $remote;
    }

    /**
     * @param string user name
     * @param string user password hash
     */
    public function setLogin($uname, $pass)
    {
        $this->username = $uname;
        $this->password_hash = $pass;

        return $this;
    }

    /**
     * Service request
     *
     * @return array responce data
     * @throws ServiceErrorException
     */
    public function request($params)
    {
        try
        {
            if(!empty($this->username)) {
                $params['username'] = $this->username;
                $params['password_hash'] = $this->password_hash;
            }

            if(!isset($params['r']))
                throw new \Exception('wrong request parameters');

            $curl = new Curl();
            $curl->get($this->remote, $params);

            if($curl->error)
                throw new \Exception('curl get fail '.$curl->error_message);

            $data = @json_decode($curl->response, true);

            $json_error = json_last_error();
            if($data === false || $json_error != JSON_ERROR_NONE)
                throw new \Exception('json fail');

            if(isset($data['status']))
            {
                if($data['status']!='ok')
                    throw new \Exception('service fail');

                return $data['body'];
            }

            return $data;
        }
        catch(\Exception $e)
        {
            throw new ServiceErrorException('Service error: '.$e->getMessage(), $e->getCode(), $e);
        }
    }

}
