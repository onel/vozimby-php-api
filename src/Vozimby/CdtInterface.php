<?php

namespace Vozimby;

/**
 * cost delivery type info
 */
interface CdtInterface
{
    /**
     * rate consts
     */
     const CDT_EXPRESS  = 1;
     const CDT_VIP      = 1;
     const CDT_STANDART = 2;
     const CDT_BUDGET   = 3;

     /**
      * calulcation type consts
      */
     const CLC_PACKAGE = 1;
     const CLC_CARGO   = 2;
}
