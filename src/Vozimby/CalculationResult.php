<?php

namespace Vozimby;

use Vozimby\Exception\NotAllowedException;

/**
 * Full calculation results
 */
class CalculationResult
{
    /**
     * rates list
     * @var array
     */
    private $rates;

    /**
     * @param array $rawData list of rates
     */
    public function __construct($rawData)
    {
        $this->rates = $rawData;
    }

    /**
     * get certain cdt
     * @param  ingeger selected cdt
     * @return array
     * @throws Vozimby\Exception\NotAllowedException
     * @see Vozimby\CdtInterface
     */
    public function getCost($costDeliveryType)
    {
        if(!isset($this->rates[ $costDeliveryType ])
        || !$this->rates[ $costDeliveryType ]['enabled']) {
            throw new NotAllowedException('rate not allowed');
        }

        return $this->rates[ $costDeliveryType ];
    }
}
