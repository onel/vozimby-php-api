<?php

namespace Vozimby;

use Vozimby\Client;
use Vozimby\CdtInterface;
use Vozimby\CalculationResult;
use Vozimby\Exception\InsufficientDataException;
use Vozimby\Exception\ServiceErrorException;

/**
 * Calculation connector
 */
class Calculator implements CdtInterface
{
    /**
     * Delivery options
     * @var array
     */
    private $options = array();

    /**
     * Cashed rules
     * @var array
     */
    private $rulesList = array();

    /**
     * Cached localiry list
     * @var array
     */
    private $locationList = array();

    /**
     * service client
     */
    private $client = null;


    /**
     * @param string http remote gate
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }


    /**
     * Set delivery option
     *
     * Common options:
     * calculation - calculation type (cargo or IM)
     * cost_delivery - budget, express, econom
     * good-cost - good cost
     * client-id - PM client UID
     *
     *
     * Delivery location options:
     * locality - cargo locality
     *
     *
     * IM options:
     * rule - IM rule
     *
     *
     * Cargo options:
     * width - cargo w
     * height - cargo h
     * length - cargo l
     * weight - cargo weight
     *
     *
     * Special service:
     * delivery18 - special service
     * driver-help - special service
     *
     *
     * @param string
     * @param integer|string
     * @return this
     */
    public function setOption($name, $value)
    {
        $this->options[ $name ] = $value;
        return $this;
    }

    /**
     * Get option by name
     * @return string
     */
    public function getOption($name)
    {
        return isset($this->options[ $name ])? $this->options[ $name ]: '';
    }


    /**
     * options by array
     *
     * @see \Vozimby\Calculator::setOption
     * @param array option array
     * @return this
     */
    public function setOptions($list)
    {
        foreach($list as $k=>$v)
            $this->setOption($k, $v);

        return $this;
    }

    /**
     * clear all options
     * @return this
     */
    public function clearOptions()
    {
        $this->options = array();
        return $this;
    }

    /**
     * all options
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }


    /**
     * Delivery cost by options
     *
     * required options
     * calculation
     * cost_delivery
     * locality
     *
     * @throws \Vozimby\ServiceErrorException
     * @throws \Vozimby\InsufficientDataException
     *
     * @return integer cost
     */
    public function getCost()
    {
        $options = $this->options;

        // check options
        if(empty($options['calculation'])
        || empty($options['cost_delivery'])
        || empty($options['locality'])
        || ($options['calculation'] == self::CLC_CARGO
            && (empty($options['width'])
            || empty($options['height'])
            || empty($options['length'])
            || empty($options['weight']))  )
        || ($options['calculation'] == self::CLC_PACKAGE
            && (empty($options['rule'])
            || empty($options['good-cost'])) )
        || ($options['cost_delivery'] == self::CDT_STANDART
            && strpos($options['locality'], 'р-н')!==false)
        ) {
            throw new InsufficientDataException('wrong parameters');
        }

        $options['r'] = 'api/calculationJson';

        return $this->client->request($options);
    }

    /**
     * Delivery days by options
     *
     * required options:
     * locality
     * cost_delivery
     *
     * @throws \Vozimby\ServiceErrorException
     * @throws \Vozimby\InsufficientDataException
     *
     * @return array list of dates
     */
    public function getDeliveryDays()
    {
        $options = $this->options;

        if(empty($options['locality'])
        || empty($options['cost_delivery'])
        || ($options['cost_delivery'] == self::CDT_BUDGET
            && strpos($options['locality'], 'р-н')!==false)
        ) {
            throw new InsufficientDataException('wrong parameters');
        }

        $rqOptions = array(
            'r'  => 'api/daybylocality',
            'id' => $options['locality'],
        );

        $response = $this->client->request($rqOptions);

        if($response['s'] != 'ok') {
            throw new ServiceErrorException( $response['reason'] );
        }


        $datesList = array();
        switch ($options['cost_delivery']) {
            case self::CDT_EXPRESS:
                $datesList = $response['nextdays'][0];
                break;

            case self::CDT_STANDART:
                $datesList = array_slice( $response['nextdays'], 0, 2 );
                break;

            // budget
            case self::CDT_BUDGET:
            default:
                $datesList = array_slice( $response['nextdays'], 0, 4 );
                break;
        }

        return $datesList;
    }

    /**
     * Get Available locations
     *
     * @return array location names
     */
    public function getAvailableLocations()
    {
        if(!$this->locationList)
        {
            $params = array('r'=>'api/clientLocality');
            $this->locationList = $this->client->request($params);
        }

        return $this->locationList;
    }

    /**
     * List of all available rates and delyvery days
     *
     * @throws InsufficientDataException
     * @return CalculationResult
     */
    public function getAllRates()
    {
        $options = $this->options;

        // check options
        if(empty($options['calculation'])
        || empty($options['locality'])
        || ($options['calculation'] == self::CLC_CARGO
            && (empty($options['width'])
            || empty($options['height'])
            || empty($options['length'])
            || empty($options['weight']))  )
        || ($options['calculation'] == self::CLC_PACKAGE
            && (empty($options['rule'])
            || empty($options['good-cost'])) )
        ) {
            throw new InsufficientDataException('wrong parameters');
        }

        $options['r'] = 'api/calculationFullJson';

        $data = $this->client->request($options);

        return new CalculationResult($data);
    }

    /**
     * Set delivery location
     * @return this
     */
    public function setLocation($locationName)
    {
        $this->setOption('locality', $locationName);
        return $this;
    }


    /**
     * Get Available IM type codes
     *
     * @return array rule id=>name
     */
    public function getAvailableImRules()
    {
        if(!$this->rulesList)
        {
            $params = array('r'=>'api/rulesJson');
            $this->rulesList = $this->client->request($params);
        }

        return $this->rulesList;
    }


    /**
     * Available cdt
     * @return array id=>lbl
     */
    public function getAvailableCostDelivery()
    {
        return array(
            self::CDT_BUDGET   => 'budget',
            self::CDT_STANDART => 'standart',
            self::CDT_EXPRESS  => 'express',
        );
    }


    /**
     * set clc option
     * @return this
     */
    public function setImRule($rule)
    {
        $this->setOption('rule', $rule);
        return $this;
    }


    /**
     * set clc option
     * @return this
     */
    public function setCostDelivery($type)
    {
        $this->setOption('cost_delivery', $type);
        return $this;
    }

    /**
     * set clc option
     * @return this
     */
    public function getCalculationType($calculation)
    {
        $this->setOption('calculation', $calculation);
        return $this;
    }
}
