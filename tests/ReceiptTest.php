<?php

namespace Vozimby;

use Vozimby\Client;
use Vozimby\Receipt;

/**
 * TODO add mock
 */
class ReceiptTest extends \PHPUnit_Framework_TestCase
{
    protected $client;


    function setUp()
    {
        $this->client = new Client('http://vozimbylk.local/index.php');
        $this->client->setLogin('phpunit', hash('sha256', 'phpunit'));
    }


    /**
     * @expectedException \Vozimby\Exception\NotFoundException
     */
    public function testNotFound()
    {
        $r = new Receipt($this->client);

        $r->setSN('notexist', 1)->getStatus();
    }


    /**
     * @expectedException \Vozimby\Exception\ServiceErrorException
     */
    public function testWrongGate()
    {
        $r = new Receipt(
            new Client('http://vozimbylk.local/fail.php')
        );

        $r->setSN('exist', 1)->getStatus();
    }


    public function testCreate()
    {
        $r = new Receipt($this->client);
        $rId = $r->setAttributes(array(
            'good_name' => 'test',
            'real_weight' => '30',
            'size_w' => '100',
            'size_l' => '100',
            'size_h' => '100',
            'additional' => 'nope',
            'receipt_calculation_type' => Receipt::CLC_CARGO,
            'good_cost' => '20000',
            'order_receive' => '20000',
            'cost_delivery_type' => Receipt::CDT_VIP,
            'date_delivery' => date('Y-m-d'),
            'who_pays' => '0',

            'receiver_fio' => 'phpunit',
            'receiver_org_name' => 'phpunit',
            'receiver_city' => '1',
            'receiver_address' => 'Беларусь, Гродно, д.1',
            'receiver_contact1_array' => array(
                '375296192411',
            ),
            'receiver_passport_s' => 'xx',
            'receiver_passport_n' => '000000000',
            'receiver_delivery18' => 1,
            'receiver_loadhelp' => 1,
        ))
        ->create();

        $this->assertGreaterThan(0, $rId);
    }


    public function testUpdate()
    {
        $r = new Receipt($this->client);
        $rId = $r->setAttributes(array(
            'good_name' => 'test',
            'real_weight' => '30',
            'size_w' => '100',
            'size_l' => '100',
            'size_h' => '100',
            'additional' => 'nope',
            'receipt_calculation_type' => Receipt::CLC_CARGO,
            'good_cost' => '20000',
            'order_receive' => '20000',
            'cost_delivery_type' => Receipt::CDT_VIP,
            'date_delivery' => date('Y-m-d'),
            'who_pays' => '0',

            'receiver_fio' => 'phpunit',
            'receiver_org_name' => 'phpunit',
            'receiver_city' => '1',
            'receiver_address' => 'Беларусь, Гродно, д.1',
            'receiver_contact1_array' => array(
                '375296192411',
            ),
            'receiver_passport_s' => 'xx',
            'receiver_passport_n' => '000000000',
            'receiver_delivery18' => 1,
            'receiver_loadhelp' => 1,
        ))
        ->create();

        $r
        ->setAttribute('size_w', '7777')
        ->update();
    }
}
