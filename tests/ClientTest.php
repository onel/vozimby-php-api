<?php

namespace Vozimby;

use Vozimby\Client;

/**
 * TODO add mock
 */
class ClientTest extends \PHPUnit_Framework_TestCase
{
    protected $client;


    function setUp()
    {
        $this->client = new Client('http://vozimbylk.local/index.php');        
    }


    /**
     * @expectedException \Vozimby\Exception\ServiceErrorException
     */
    public function testWrongParam()
    {
        $this->client->request(array('fake'=>'1'));
    }


    /**
     * @expectedException \Vozimby\Exception\ServiceErrorException
     */
    public function testWrongUrl()
    {
        $this->client->request(array('r'=>'fake'));
    }


    public function testClientResponce()
    {
        $this->assertTrue( count($this->client->request(array('r'=>'api/rulesJson')))>0 );
    }
}
