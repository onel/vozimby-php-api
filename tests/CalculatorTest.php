<?php

namespace Vozimby;

use Vozimby\Client;
use Vozimby\Calculator;

/**
 * TODO add mock
 */
class CalculatorTest extends \PHPUnit_Framework_TestCase
{
    protected $client;
    protected $calculator;


    function setUp()
    {
        $this->client = new Client('http://vozimbylk.local/index.php');
        $this->calculator = new Calculator($this->client);
    }

    function testRules()
    {
        $this->assertTrue( count($this->calculator->getAvailableImRules())>0 );
    }

    function testLocations()
    {
        $this->assertTrue( count($this->calculator->getAvailableLocations())>0 );
    }

    function testCalculation()
    {
        //$this->calculator->setOption('client-id', 777);
        $this->calculator->setOption('calculation', Calculator::CLC_CARGO);
        $this->calculator->setOption('cost_delivery', Calculator::CDT_STANDART);
        $this->calculator->setOption('locality', 'Минск (Минская .обл)');
        $this->calculator->setOption('good-cost', 100);
        $this->calculator->setOption('rule', 147);
        $this->calculator->setOption('width', 1);
        $this->calculator->setOption('height', 1);
        $this->calculator->setOption('length', 1);
        $this->calculator->setOption('weight', 1);
        $this->calculator->setOption('delivery18', 1);
        $this->calculator->setOption('driver-help', 1);

        $this->assertGreaterThan(10000, $this->calculator->getCost());
    }

    function testClearOptions()
    {
        $this->calculator->setOption('good-cost', 100);
        $this->calculator->clearOptions();
        $this->assertTrue( count($this->calculator->getOptions())==0 );
    }

    /**
     * @expectedException \Vozimby\Exception\InsufficientDataException
     */
    function testBaseParams()
    {
        $this->calculator->clearOptions();
        $this->calculator->setOption('calculation', Calculator::CLC_CARGO);
        $this->calculator->setOption('cost_delivery', Calculator::CDT_STANDART);
        // $this->calculator->setOption('locality', 'Минск (Минская .обл)');
        $this->calculator->setOption('good-cost', 100);
        $this->calculator->setOption('rule', 147);
        $this->calculator->setOption('width', 1);
        $this->calculator->setOption('height', 1);
        $this->calculator->setOption('length', 1);
        $this->calculator->setOption('weight', 1);
        $this->calculator->setOption('delivery18', 1);
        $this->calculator->setOption('driver-help', 1);

        $this->calculator->getCost();
    }

    /**
     * @expectedException \Vozimby\Exception\InsufficientDataException
     */
    function testEconom()
    {
        $this->calculator->clearOptions();
        $this->calculator->setOption('calculation', Calculator::CLC_CARGO);
        $this->calculator->setOption('cost_delivery', Calculator::CDT_BUDGET);
        $this->calculator->setOption('locality', 'Межа (Городокский р-н, Витебская)');
        $this->calculator->setOption('good-cost', 100);
        $this->calculator->setOption('rule', 147);
        $this->calculator->setOption('width', 1);
        $this->calculator->setOption('height', 1);
        $this->calculator->setOption('length', 1);
        $this->calculator->setOption('weight', 1);

        $this->calculator->getCost();
    }

    /**
     * @expectedException \Vozimby\Exception\InsufficientDataException
     */
    function testCargoCalculator()
    {
        $this->calculator->clearOptions();
        $this->calculator->setOption('calculation', Calculator::CLC_CARGO);
        $this->calculator->setOption('cost_delivery', Calculator::CDT_BUDGET);
        $this->calculator->setOption('locality', 'Межа (Городокский р-н, Витебская)');
        $this->calculator->setOption('good-cost', 100);
        $this->calculator->setOption('rule', 147);
        $this->calculator->setOption('width', 1);
        $this->calculator->setOption('height', 1);
        $this->calculator->setOption('length', 1);
        // $this->calculator->setOption('weight', 1);

        $this->calculator->getCost();
    }

    /**
     * @expectedException \Vozimby\Exception\InsufficientDataException
     */
    function testImCalculator()
    {
        $this->calculator->clearOptions();
        $this->calculator->setOption('calculation', Calculator::CLC_CARGO);
        $this->calculator->setOption('cost_delivery', Calculator::CDT_BUDGET);
        $this->calculator->setOption('locality', 'Межа (Городокский р-н, Витебская)');
        $this->calculator->setOption('good-cost', 100);
        // $this->calculator->setOption('rule', 147);
        $this->calculator->setOption('width', 1);
        $this->calculator->setOption('height', 1);
        $this->calculator->setOption('length', 1);
        $this->calculator->setOption('weight', 1);

        $this->calculator->getCost();
    }

    /**
     * @expectedException \Vozimby\Exception\InsufficientDataException
     */
    function testFullRatesInsufficientData()
    {
        $this->calculator->clearOptions();
        $this->calculator->setOption('calculation', Calculator::CLC_CARGO);
        //$this->calculator->setOption('locality', 'Межа (Городокский р-н, Витебская)');
        $this->calculator->setOption('good-cost', 100);
        $this->calculator->setOption('rule', 147);
        $this->calculator->setOption('width', 1);
        $this->calculator->setOption('height', 1);
        $this->calculator->setOption('length', 1);
        $this->calculator->setOption('weight', 1);

        $result = $this->calculator->getAllRates();
    }


    function testFullRatesSuccess()
    {
        $this->calculator->clearOptions();
        $this->calculator->setOption('calculation', Calculator::CLC_CARGO);
        $this->calculator->setOption('locality', 'Межа (Городокский р-н, Витебская)');
        $this->calculator->setOption('good-cost', 100);
        $this->calculator->setOption('rule', 147);
        $this->calculator->setOption('width', 1);
        $this->calculator->setOption('height', 1);
        $this->calculator->setOption('length', 1);
        $this->calculator->setOption('weight', 1);

        $result = $this->calculator->getAllRates();

        $this->assertTrue(( $result instanceof \Vozimby\CalculationResult ));
    }

    function testFullRatesData()
    {
        $this->calculator->clearOptions();
        $this->calculator->setOption('calculation', Calculator::CLC_CARGO);
        $this->calculator->setOption('locality', 'Межа (Городокский р-н, Витебская)');
        $this->calculator->setOption('good-cost', 100);
        $this->calculator->setOption('rule', 147);
        $this->calculator->setOption('width', 1);
        $this->calculator->setOption('height', 1);
        $this->calculator->setOption('length', 1);
        $this->calculator->setOption('weight', 1);

        $result = $this->calculator->getAllRates();

        $express = $result->getCost(\Vozimby\CdtInterface::CDT_EXPRESS);

        $this->assertTrue($express > 0);
    }

    /**
     * @expectedException \Vozimby\Exception\NotAllowedException
     */
    function testFullRatesNotAllowed()
    {
        $this->calculator->clearOptions();
        $this->calculator->setOption('calculation', Calculator::CLC_CARGO);
        $this->calculator->setOption('locality', 'Межа (Городокский р-н, Витебская)');
        $this->calculator->setOption('good-cost', 100);
        $this->calculator->setOption('rule', 147);
        $this->calculator->setOption('width', 1);
        $this->calculator->setOption('height', 1);
        $this->calculator->setOption('length', 1);
        $this->calculator->setOption('weight', 1);
        $this->calculator->setOption('good-cost', 1);

        $result = $this->calculator->getAllRates();

        $express = $result->getCost(999);
    }
}
