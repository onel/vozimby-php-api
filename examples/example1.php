<?php
require_once 'vendor/autoload.php';

use Vozimby\Client;
use Vozimby\Calculator;

$client = new Client('http://b2b.vozim.by/index.php');
$clc    = new Calculator($client);

// все доступные категории грузов
print_r($clc->getAvailableImRules());

// все доступные локации доставки (текстовое представление)
print_r($clc->getAvailableLocations());

// пример расчета стоимости доставки
$cost = $clc->setOptions(array(
            'calculation'=>Calculator::CLC_PACKAGE,
            'cost_delivery'=>Calculator::CDT_STANDART,
            'good-cost'=>100000,
            'locality'=>'Минск (Минская .обл)',
            'rule'=>243, // код категории
            'width'=>123,
            'height'=>123,
            'length'=>123,
            'weight'=>123,
            'delivery18'=>1,
            'driver-help'=>1,
        ))
        ->getCost();

var_dump($cost.' BYR');


// дни доставки по Минску, тариф "стандарт" (установили выше)
$days = $clc->getDeliveryDays();

var_dump($days);
